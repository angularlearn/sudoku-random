import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'sr-block',
  templateUrl: './block.component.html',
  styleUrls: ['./block.component.less']
})
export class BlockComponent implements OnInit {

  @Input() blockValues: number[];

  constructor() { }

  ngOnInit() {}

}
