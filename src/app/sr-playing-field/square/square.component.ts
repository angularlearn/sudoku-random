import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'sr-square',
  templateUrl: './square.component.html',
  styleUrls: ['./square.component.less']
})
export class SquareComponent implements OnInit {
  @Input() squareValue: number;

  constructor() { }

  ngOnInit() {}

}
