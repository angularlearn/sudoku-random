import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FieldComponent } from './sr-playing-field/field/field.component';
import { BlockComponent } from './sr-playing-field/block/block.component';
import { SquareComponent } from './sr-playing-field/square/square.component';

@NgModule({
  declarations: [
    AppComponent,
    FieldComponent,
    BlockComponent,
    SquareComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
